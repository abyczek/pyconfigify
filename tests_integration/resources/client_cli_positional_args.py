import sys
from pyconfigify.configify import configify, Setting
this = sys.modules[__name__]

this.COMMAND = Setting("start", "Dummy 1st parameter", pos=1, name='COMMAND', choices=['start', 'stop', 'restart'])
# TODO NOT HANDLING NUMBERS
this.LOG_LEVEL = Setting('2', "Dummy 2nd positional parameter", name='LOG_LEVEL', pos=2, choices=['1','2','3'])
this.MESSAGE = Setting("default-message", "optional cli arg")

def main():
    
    configify(this)
    print_script_vars()

def print_script_vars():
    print "COMMAND: {} LOG_LEVEL: {} MESSAGE: {}".format(this.COMMAND, this.LOG_LEVEL, this.MESSAGE)

if __name__ == "__main__":
    main()

