# OVERVIEW

Configify project goal is to update easily any script's parameters with values from external configuration file or 
command line interface arguments.

This allows to get rid of cross cutting concerns such as:

- building command line interface, including help
- parsing CLI input
- loading and parsing external config file

from client script and stay focused on "business logic".

Unfortunate thing is that parameters are not just simple values. They have number of attributes such as

- name
- type
- description (which is also part of CLI help)

For that reason there is necessity to define descriptors for each parameter subjected to "configifying". 

**Current implementation note:**
There is a number of ways to define such a descriptor, like docstring, external structure accompanying parameters etc.
However first approach assumes configuration parameter initial value to be descriptor itself. 
Since from the client script point of view, configuration parameter cannot by anything like descriptor 
or other kind of metadata, this implies that configifying process transforms data describing configuration parameters to 
raw values coming from config file, cli args or default ones.
```
config_object.PARAMETER1 = ("default value", "parameter description")

configify(config_object)

# after having script configifyied (i.e. loaded cli args and/or config file):

config_object.PARAMETER1 == "some value"
```

# DESIGN n ARCHITECTURE

## Concepts
For the sake of technical documentation clarity, there are some concepts defined within this project:

__client script__ : parametrized script calling _configify_
__config__ or __config object__: any object, which is a container for collection of "parameters" (see *config attribute* definition below). 
It is actually data passed from *client script* to configify to process it.
Note: *config object* could be defined as module and module scope variables:
```this = sys.modules[__name__]```
__config attribute__ : this is *config object's* attribute holding "parameter" and its metadata.
__setting__ : is actual definition and description of *client script* "parameter", hence *setting* is a type of *config attribute*.
Technically *setting* is represented by ```Setting``` class.
__cli args__ : command line input matching *settings*
__config file__ : external file with set of parameters for the *client script*
__parameter__ : has no strict technical representation, however it often means a value under some name.

## CONFIGIFY PROCESS

Configifing *client script* logically could be defined as:

1. Passing collection of *settings* held by single object from *client script* to *configiy* function.
2. Loading and parsing parameters from external configuration file.
2.1 Overwriting *settings* default values.
3. Building command line interface from the set of *settings* and parsing (input).
3.1 Overwriting default values and config file values.
4. Updating *config attributes* with raw values. From now on *client script* has no *settings*, just parameters values.

## FUTURE IMPROVEMENTS 

__definition of settings__ (parameters metadata) : as configifing implies side effect not only by changing parameters value but also type of this value,
i.e. from descriptor to raw value. Alternative representation of Settings could include some soft of annotations or docstrings 

__remote access__ :  first approach is limited only to overwriting parameters from config file or command line input. From devops point of view it could 
make sense to load data data from remote location.

# CONFIG FILE

Currently configuration file is a YAML file with entries case-insensitive matching *config attributes*
For example for the attribute:
```
this.LOG_DIR = Setting('/var/log', 'logs directory')
```
configuration file would look like:
```
log_dir: /home/myuser/myapp/logs
```

__Important__ thing is the file location is parametrized itself by Setting with the name: 'config_file', which implies
that config file is defined in the following way:
```
$ client_script.py --config_file /example/dir/configfile.yaml
```

name of cli arg for 'config_file' can be overwritten by passing new name to configify function.
If configify is called in the following way:
```
configify(config_obj, 'cfg_file')
```
then script invocation will be:
```
$ client_script.py --cfg_file /example/dir/configfile.yaml
or
$ client_script.py -cf /example/dir/configfile.yaml
```

# CLI

Command Line Interface is build with [argparse](https://docs.python.org/2.7/library/argparse.html) library.

## CLI arguments
CLI arg names are made from *Settings*/*config attributes* lower cased names.
In addition short names are formed by concatenating first letters of each workd separated by '_' (underscore character).

Example:
Config attribute ```LOGS_DIR``` could be set via CLI as:
```./my_script.py --logs_dir /home/myuser/logs```
or
```./my_script.py -ld /home/myuser/logs```


# EXAMPLE

Having client script like:
```
this.APP_NAME = Setting("TestScript", "Application name.")
this.APP_HOME = Setting("/opt/testscript", "Application base directory.")
this.POOL_SIZE = Setting(7, "Dummy pool size parameter of int type")

configify(this)

print "APP_NAME: {} APP_HOME: {} POOL_SIZE: {}".format(this.APP_NAME, this.APP_HOME, this.POOL_SIZE)
```

and config file like:
```
app_name: app_name_from_file
APP_HOME: /app_home/from_file
```
then script execution
```
$ client_script.py -cf /path/to/config_file.yaml -an NewAppName --app_home /new_home
```
will result in script parameter values to be set:
```
this.APP_NAME = NewAppName
this.APP_HOME = /new_home   # note even if config file contains this parameter, it is overwritten by cli arg
this.POOL_SIZE = 7          # remains default value
```
