"""
"configify" is a utility module to expose script's configuration parameters for an easy update.

Basic usage is that a "client script" passes "config object", which is a container for a set of parameters
to "configify", that is overwrite them with values coming from either external config file or command line arguments.

In context of configify module, individual "parameters" are represented by ```Setting``` class, which is a value and
also descriptor, of which command line arguments is being built and config file parsed.

Once configifying process is completed parameters are not ```Settings``` any longer, but raw values.


Example:
    # client script
    config.LOG_DIR = ("/var/log", "Logs base directory.")

    configify(config)

    # user executes script
    $ ./my_script.py --log_dir /home/myuser/logs

    # client scripts continues execution
    config.LOG_DIR == "/home/myuser/logs" # => True

"""

import sys
from os import path
import argparse
import yaml

this = sys.modules[__name__]

"""CLI argument default name to specify optional configuration file."""
this.CONFIG_FILE = "config_file"

class Setting(object):
    """Configuration setting descriptor.

    Setting consist of the following attributes:
    - val : Value of setting
    - desc : Description of setting, used for CLI help
    - name : Name of the setting, this value is the same as configuration object attribute name
    - short : Short name of the setting, used for CLI
    - pos : Position of the setting, used by CLI
    - choices : List of available values for CLI positional arguments
    - switch : Flag converting optional argument into switch. If set to True then Setting returns value of the 'val' otherwise None
    """

    def __init__(self, val, desc=None, **kwargs):

        self.val = val
        self.desc = desc
        self.name = None
        self.short = None
        self.pos = sys.maxint
        self.choices = None
        self.switch = False

        self.__dict__.update(kwargs)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


def configify(config_obj, cli_config_file = this.CONFIG_FILE):
    """The only API function to process configifying.

    configify function updates `config_obj` object, by reassigning raw value to attributes identified as
    configuration settings.

    Args:
        config_obj: any object, which is container of all attributes defining settings.
        cli_config_file: cli arg name to specify configuration file path

    Example:
        obj.SETTING1 = Setting("val1", "This is short desc for cli help")
        obj.setting2 = Setting(3, "Positional (required) cli arg", pos=1, choices=[1,2,3])

        configify(obj)

        obj.SETTING1 == "val1"
        obj.setting2 == 3
    """

    settings = _get_settings(config_obj)

    settings = _add_implicit_settings(settings, cli_config_file)

    cli_args = _parse_cli_args(settings)

    file_path = _get_config_file_path(cli_args, cli_config_file)

    file_args = _load_config_file(file_path) if file_path else None

    settings = _apply_config_file(settings, file_args)

    settings = _apply_cli_args(settings, cli_args)

    _apply_settings(config_obj, settings)

def _get_settings(config_obj):
    """Extracts configuration settings from `config_obj`.

    Configuration setting is a `config_obj` attribute of Setting type, which name is not starting with '__'.
    In addition, Setting's 'name' attribute is being set with `config_obj` attribute name.
    """

    def is_setting(attr_name):
        return isinstance(getattr(config_obj, attr_name), Setting) and not attr_name.startswith("__")

    def get_setting(attr_name):
        setting = getattr(config_obj, attr_name)
        setting.name = attr_name
        return setting

    settings = map(
        get_setting,
        filter(is_setting, dir(config_obj))
    )

    return settings

def _add_implicit_settings(settings, config_file):

    if config_file:
        settings.append(Setting(None, "Config file path", name=config_file))
    return settings

def _parse_cli_args(settings):
    """Builds argparse.ArgumentParser according to `settings` description and performs CLI arguments parsing.

    Regardless of Settings casing, CLI arguments are lowercased.

    Note: argparse.ArgumentParser requires to define arguments as '--some_arg_name', and short name as '-x'.
    To avoid short names easy conflicts, short names are generated as first letters of each word of setting name.
    If short name is explicitly defined then it is not updated.

    Example:
        Setting('val1', '', name=first_setting)
        translates to '-fs'

        TODO:
        Setting('val2, '', name=SecondArg)
        translates to '-sa'

        Setting('val3, '', name=simple)
        translates to '-l'

        Setting('val4', '', name=last_arg, short=x)
        translates to '-x'

    Returns:
        object containing parsed input arguments.
    """

    def get_arg_name(name):
        return "--" + name.lower()

    def get_arg_shortname(s):
        if s.pos < sys.maxint:
            return None

        if s.short is not None:
            return "-" + s.short

        return "-" + "".join(
            map(
                lambda x: x.lower()[0],
                filter(None,s.name.split("_"))
            )
        )

    def reducer(p, s):
        if s.choices is None:
            p.add_argument(
                get_arg_shortname(s),
                get_arg_name(s.name),
                help=s.desc,
                default=None,
                action='store_true' if s.switch else None
                # todo type=int if isinstance(s.val, int) else str
            )
        else:
            p.add_argument(
                s.name.lower(),
                choices=s.choices,
                help=s.desc,
                default=None
                # TODO type=int if isinstance(s.val, int) else str
            )

        return p

    parser = reduce(reducer,
                    sorted(settings, key=lambda x: x.pos),
                    argparse.ArgumentParser())

    return parser.parse_args()

def _get_config_file_path(config, config_file):
    """Resolves full path to configuration file.
    If the given path is relative, then base dir is sys.argv[0]

    Function searches for `this.CONFIG_FILE` attribute, which should have either fully qualified path to config file
    or relative path.

    Args:
        config: output of CLI parsing.

    Returns:
        Full path to config file.
    """

    if not hasattr(config, config_file):
        return None

    config_file = getattr(config, config_file)

    if not config_file:
        return None

    if path.isabs(config_file):
        return config_file

    return path.join(path.dirname(sys.argv[0]), config_file)

def _load_config_file(file):
    """Loads YAML configuration file.

    Args:
        file (str): full path to config file.

    Returns:
        dictionary with data to override settings: name and value
    """

    args_dict = None
    with open(file, 'r') as stream:
        try:
            args_dict = yaml.load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    return args_dict

def _apply_config_file(settings, file_args):
    """Overwrites `settings` list with data from config file.

    Args:
        settings: list of Settings
        file_arg: dictionary of settings coming from configuration file.

    Returns:
        list of overwritten Settings
    """

    if not file_args:
        return settings

    def f(s):
        s.val = ([file_args[k] for k in file_args if k.lower() == s.name.lower()] or [None])[0] or s.val
        return s

    settings = map(f, settings)
    return settings

def _apply_cli_args(settings, cli_args):
    """Overwrites `settings` list with data from CLI args.

    Args:
        settings: list of Settings
        cli_args: object containing parsed parameters from command line interface input.

    Returns:
        list of overwritten Settings
    """

    def f(s):
        cli_val = next((getattr(cli_args, k) for k in cli_args.__dict__.keys() if k.lower() == s.name.lower()), None)

        if not s.switch:
            s.val = cli_val or s.val
        else:
            s.val = s.val if cli_val else None

        return s

    return map(f, settings)

def _apply_settings(config_obj, settings):
    """Applies final set of configuration settings to `config_obj`"""

    def reducer(cfg_obj, s):
        setattr(cfg_obj, s.name, s.val)
        return cfg_obj

    config_obj = reduce(reducer, settings, config_obj)
    return config_obj









