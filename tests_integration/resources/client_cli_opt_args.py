import sys
from pyconfigify.configify import configify, Setting
this = sys.modules[__name__]

this.APP_NAME = Setting("TestScript", "Application name.")
this.APP_HOME = Setting("/opt/testscript", "Application base directory.")
this.POOL_SIZE = Setting(7, "Dummy pool size parameter of int type")

this.APP_SWITCH = Setting("--debug", "Optional arg without value - switch", switch=True)

def main():

    configify(this)
    print_script_vars()

def print_script_vars():
    print "APP_NAME: {} APP_HOME: {} POOL_SIZE: {} APP_SWITCH: {}".format(this.APP_NAME, this.APP_HOME, this.POOL_SIZE, this.APP_SWITCH)

if __name__ == "__main__":
    main()

