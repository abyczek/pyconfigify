import sys
import os
import os.path

import unittest2 as unittest
import mock

from pyconfigify.configify import (
    _get_settings,
    _add_implicit_settings,
    _parse_cli_args,
    _get_config_file_path,
    _load_config_file,
    _apply_config_file,
    _apply_cli_args,
    _apply_settings,
    configify,
    Setting
)

class Expando(object):
    """Class for dummy objects holding different kind of data."""

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

class Tests_get_settings(unittest.TestCase):
    """_get_settings function tests."""

    def setUp(self):
        self.configs = Expando()
        self.configs.APP_NAME = Setting("MyApp", "Dummy App Name.")

    def test_empty_settings(self):
        """Returns empty list for input without settings."""

        self.assertEqual([], _get_settings(Expando))

    def test_simple_setting(self):
        """Extracts simple settings with the only required fields.
        'name' attribute is set to attribute name.
        Other Setting's optional attributes are initialized with None.
        """

        settings = _get_settings(self.configs)
        self.assertEqual(1, len(settings))

        self.assertEqual("APP_NAME", settings[0].name)
        self.assertEqual("MyApp", settings[0].val)
        self.assertEqual("Dummy App Name.", settings[0].desc)
        self.assertIsNone(settings[0].short)
        self.assertEqual(sys.maxint, settings[0].pos)
        self.assertIsNone(settings[0].choices)

    def test_settings_with_optional_attributes(self):
        """Test all optional attributes."""

        self.configs.APP_NAME = Setting("valX", "Dummy App Name.",
                                        short="ShortName",
                                        pos=7,
                                        choices=["val1", "val2"])

        setting = _get_settings(self.configs)[0]

        self.assertEqual("APP_NAME", setting.name)
        self.assertEqual("valX", setting.val)
        self.assertEqual("Dummy App Name.", setting.desc)
        self.assertEqual("ShortName", setting.short)
        self.assertEqual(7 , setting.pos)
        self.assertItemsEqual(["val1", "val2"], setting.choices)

    def test_settings_with_name_different_casing(self):
        """Test Setting name casing.

        Setting attribute name allows upper and lower casing (no naming convention).
        """

        self.configs.AppName2 = Setting("AppName2Val", "CamelCase Attr Name.")

        settings = _get_settings(self.configs)
        self.assertEqual(2, len(settings))

        self.assertEqual(Setting("MyApp", "Dummy App Name.", name="APP_NAME"), settings[0])
        self.assertEqual(Setting("AppName2Val", "CamelCase Attr Name.", name="AppName2"), settings[1])

    def test_when_config_attriute_which_is_not_of_setting_type_then_it_ignored(self):
        """Config attributes of types different that Setting are ignored."""

        self.configs.APP_NAME = "NotASetting"

        settings = _get_settings(self.configs)
        self.assertEqual(0, len(settings))

    def test_when_config_attribute_name_starting_with_underscores_then_is_ignored(self):
        """Settings with name starting with __ are ignored."""

        self.configs.__APP_NAME2__ = Setting("MyApp2", "Dummy App Name2.")

        settings = _get_settings(self.configs)
        self.assertEqual([Setting("MyApp", "Dummy App Name.", name="APP_NAME")], settings)


class Tests_add_implicit_settings(unittest.TestCase):
    """Tests appending default settings.

    Default setting is config file cli argument."""

    def test_add_implicit_settings(self):

        settings = _add_implicit_settings([], "config_file")
        self.assertEqual([Setting(None, "Config file path", name="config_file")], settings)

    def test_when_config_file_is_none_then_add_default_settings_adds_nothing(self):

        settings = _add_implicit_settings([], None)
        self.assertEqual([], settings)

    def test_when_config_file_is_empty_then_add_default_settings_adds_nothing(self):

        settings = _add_implicit_settings([], '')
        self.assertEqual([], settings)


class Tests_parse_cli_args(unittest.TestCase):
    """_parse_cli_args function tests."""

    def setUp(self):
        self.settings = [
            Setting("Value1", "This is upper cased setting.", name="SETTING1"),
        ]

    @mock.patch('sys.argv', ["dummy_script.py", "--setting1", "val1"])
    def test_parse_cli_optional_args(self):
        """Test CLI optional argument."""

        args = _parse_cli_args(self.settings)

        self.assertEqual("val1", args.setting1)

    @mock.patch('sys.argv', ["dummy_script.py", "-s", "set1",  "-ap", '777'])
    def test_parse_optional_cli_args_with_shortnames(self):
        """Test CLI args specified by short names."""

        self.settings.append(Setting('123', "This is camel cased numerical setting.", name="ANOTHER_PARAM"))

        args = _parse_cli_args(self.settings)

        self.assertEqual("set1", args.setting1)
        self.assertEqual("777", args.another_param)

    @mock.patch('sys.argv', ["dummy_script.py", "-s", "set1",  "-ap"])
    def test_parse_optional_switch_on_cli_args(self):
        """Test switch is ON. When switch is enabled (present) then parsed arg returns True"""

        self.settings.append(Setting('swtich_val', "This switch value.", name="ANOTHER_PARAM", switch=True))

        args = _parse_cli_args(self.settings)

        self.assertEqual("set1", args.setting1)
        self.assertEqual(True, args.another_param)

    @mock.patch('sys.argv', ["dummy_script.py", "-s", "set1"])
    def test_parse_optional_switch_off_cli_args(self):
        """Test switch is OFF. When switch is disabled then parsed arg returns None"""

        self.settings.append(Setting('swtich_val', "This switch value.", name="ANOTHER_PARAM", switch=True))

        args = _parse_cli_args(self.settings)

        self.assertEqual("set1", args.setting1)
        self.assertEqual(None, args.another_param)

    @mock.patch('sys.argv', ["dummy_script.py", "--setting1", "set1",  "--param2", "parameter2"])
    def test_parse_cli_args_case_insensitive(self):
        """Test CLI args with different casing than Settings metadata."""

        self.settings.append(Setting(123, "This is camel cased numerical setting.", name="Param2"))

        args = _parse_cli_args(self.settings)

        self.assertEqual("set1", args.setting1)
        self.assertEqual("parameter2", args.param2)

    @mock.patch('sys.argv', ["dummy_script.py", "command3"])
    def test_parse_cli_positional_arg(self):
        """Test positional (required) argument."""

        self.settings = [(Setting("command2", "This is positional argument", name="command",
                                  pos=0, choices=["command1", "command2", "command3"]))]

        args = _parse_cli_args(self.settings)
        self.assertEqual("command3", args.command)

    @mock.patch('sys.argv', ["dummy_script.py", "action2", "command2"])
    def test_parse_two_cli_positional_args(self):
        """Test multiple positional arguments."""

        self.settings = [(Setting("command2", "This is positional argument", name="command",
                                  pos=5, choices=["command1", "command2", "command3"])),
                         (Setting("action2", "This is second positional argument", name="action",
                                  pos=3, choices=["action1", "action2", "action3"]))]

        args = _parse_cli_args(self.settings)
        self.assertEqual("command2", args.command)
        self.assertEqual("action2", args.action)

    @mock.patch('sys.argv', ["dummy_script.py", "commandX"])
    def test_when_cli_positional_arg_have_invalid_value_then_ex_thrown(self):
        """Test positional argument invalid value, that is value not defined on ```choices``` list."""

        self.settings = [(Setting("command2", "This is positional argument", name="command",
                                  pos=0, choices=["command1", "command2", "command3"]))]

        try:
            _parse_cli_args(self.settings)
            self.fail("Exception not thrown")
        except SystemExit as ex:
            print ex.args

    @mock.patch('sys.argv', ["dummy_script.py", "command2", "action2"])
    def test_when_two_cli_positional_args_in_wrong_order_then_ex_thrown(self):
        """Test wrong sequence of positional arguments."""

        self.settings = [(Setting("command2", "This is positional argument", name="command",
                                  pos=5, choices=["command1", "command2", "command3"])),
                         (Setting("action2", "This is second positional argument", name="action",
                                  pos=3, choices=["action1", "action2", "action3"]))]

        try:
            args = _parse_cli_args(self.settings)
            self.fail("Exception not thrown")
        except SystemExit:
            pass

    @mock.patch('sys.argv', ["dummy_script.py"])
    def test_when_cli_arg_is_not_provided_then_parse_cli_optional_args_returns_none_value(self):
        """Each setting not set via cli args is defaulted to None"""

        args = _parse_cli_args(self.settings)

        self.assertEqual(None, args.setting1)


class Tests_get_config_file_path(unittest.TestCase):
    """_get_config_file_path tests"""

    def setUp(self):
        self.base_dir = os.path.dirname(sys.argv[0])

    def test_absolute_path(self):
        """When given full path then it returns the same full path."""

        expando = Expando(config_file="/root/dir/config.yaml")

        path = _get_config_file_path(expando, 'config_file')
        self.assertEqual("/root/dir/config.yaml", path)

    def test_when_no_attribute_with_config_file_path_then_returns_none(self):
        """If there is no cofing_file parameter then returns None."""

        expando = Expando(config_file_x = "/my/home/config.yaml")

        self.assertIsNone(_get_config_file_path(expando, 'config_file'))

    def test_relative_path(self):
        """Resolves full path from relative path with the base dir set to entry point script location"""

        expando = Expando(cfg_file="dir/config.yaml")

        path = _get_config_file_path(expando, 'cfg_file')

        expected = os.path.join(self.base_dir, 'dir/config.yaml')

        self.assertEqual(expected, path)


class Tests_load_config_file(unittest.TestCase):
    """_load_config_file tests.

    Note: this test does not mock yaml file loading.
    """

    def setUp(self):
        self.base_dir = os.path.dirname(os.path.realpath(__file__))
        self.resources_dir = os.path.join(self.base_dir, "resources")
        self.simple_test_data_file = os.path.join(self.resources_dir, "simple_test_data.yaml")


    def test_load_simple_config_file(self):
        """Test loading simple YAML configuration file"""

        args = _load_config_file(self.simple_test_data_file)

        self.assertEqual("/my/home", args["app_home"])
        self.assertEqual("my_app", args["app_name"])


class Tests_apply_config_file(unittest.TestCase):
    """_apply_config_file tests."""

    def test_apply_file_arg(self):
        """Test overwriting Settings with config file."""

        settings = [Setting("val1", "desc1", name="set1")]

        file_args = {'set1': "val1B"}
        applied = _apply_config_file(settings, file_args)

        self.assertEqual(Setting("val1B", "desc1", name="set1"), applied[0])

    def test_apply_file_arg_with_not_matching_casing(self):
        """Test overwriting Settings with config file, where casing of file entries are not matching Settings names."""

        settings = [Setting("val1", "desc1", name="set1")]

        file_args = {'SET1': "val1B"}
        applied = _apply_config_file(settings, file_args)

        self.assertEqual(Setting("val1B", "desc1", name="set1"), applied[0])

    def test_apply_empty_file_arg(self):
        """Test overwriting Settings with empty set of CLI args, output is the same as input."""

        settings = [Setting("val1", "desc1", name="set1")]

        file_args = {}
        applied = _apply_config_file(settings, file_args)

        self.assertEqual(Setting("val1", "desc1", name="set1"), applied[0])

    def test_apply_file_arg_to_empty_settings(self):
        """Test overwriting empty Settings list, output is the same as input (empty)"""

        settings = []

        file_args = {'set1': "val1B"}
        applied = _apply_config_file(settings, file_args)

        self.assertEqual([], applied)


class Tests_apply_cli_args(unittest.TestCase):
    """_apply_cli_args tests."""

    def test_apply_cli_arg(self):
        """Test overwriting Settings with CLI args."""

        cli_args = Expando(set1="val1B")
        settings = [Setting("val1", "desc1", name="set1")]

        applied = _apply_cli_args(settings, cli_args)

        self.assertEqual(Setting("val1B", "desc1", name="set1"), applied[0])

    def test_apply_cli_arg_for_switch_on(self):
        """If Setting is a switch and switch is ON then it returns value."""

        cli_args = Expando(set1="val1B")
        settings = [Setting("val1", "desc1", name="set1", switch=True)]

        applied = _apply_cli_args(settings, cli_args)

        self.assertEqual("val1", applied[0].val)

    def test_apply_cli_arg_for_switch_off(self):
        """If Setting is a switch and switch is ON then it returns value."""

        cli_args = Expando(set1="val1B")
        settings = [Setting("val1", "desc1", name="set1", switch=False)]

        applied = _apply_cli_args(settings, cli_args)

        self.assertEqual('val1B', applied[0].val)

    def test_apply_cli_arg_with_not_matching_casing(self):
        """Test overwriting Settings with CLI args with different casing."""

        cli_args = Expando(SET1="val1B")
        settings = [Setting("val1", "desc1", name="set1")]

        applied = _apply_cli_args(settings, cli_args)

        self.assertEqual(Setting("val1B", "desc1", name="set1"), applied[0])

    def test_apply_empty_cli_arg(self):
        """Test overwriting Settings with empty set of CLI args, input and output is the same."""

        cli_args = Expando()
        settings = [Setting("val1", "desc1", name="set1")]

        applied = _apply_cli_args(settings, cli_args)

        self.assertEqual(Setting("val1", "desc1", name="set1"), applied[0])


class Tests_apply_settings(unittest.TestCase):
    """_apply_settings test."""

    def test_apply_simple_setting(self):
        """Test config object update with Settings."""

        configs = Expando()
        configs.APP_NAME = Setting("MyApp", "Dummy App Name.")

        settings = [Setting("MyApp2", "Dummy App Name2.", name="APP_NAME")]

        _apply_settings(configs, settings)

        self.assertEqual("MyApp2", configs.APP_NAME)

    def test_apply_setting_case_sensitive(self):
        """Test config object update with Settings - different casing"""

        configs = Expando()
        configs.APP_NAME = Setting("MyApp", "Dummy App Name.")

        settings = [Setting("MyApp2", "Dummy App Name2.", name="app_name")]

        _apply_settings(configs, settings)

        self.assertEqual("MyApp2", configs.app_name)

    def test_apply_empty_setting(self):
        """Test config object update with empty Settings list - input and output is the same."""

        configs = Expando()
        configs.APP_NAME = Setting("MyApp", "Dummy App Name.")

        settings = []

        _apply_settings(configs, settings)

        self.assertEqual("MyApp", configs.APP_NAME.val)

    def test_apply_setting_patially(self):
        """Test config object some (not all) attributes update with Settings."""

        configs = Expando()
        configs.APP_NAME = Setting("MyApp", "Dummy App Name.")
        configs.APP_HOME = Setting("MyHome", "Dummy App Home.")

        settings = [Setting("MyApp2", "Dummy App Name2.", name="APP_NAME")]

        _apply_settings(configs, settings)

        self.assertEqual("MyApp2", configs.APP_NAME)
        self.assertEqual("MyHome", configs.APP_HOME.val)


class Test_configify(unittest.TestCase):
    """configify function tests."""


    @mock.patch('pyconfigify.configify._get_settings')
    @mock.patch('pyconfigify.configify._add_implicit_settings')
    @mock.patch('pyconfigify.configify._parse_cli_args')
    @mock.patch('pyconfigify.configify._get_config_file_path')
    @mock.patch('pyconfigify.configify._load_config_file')
    @mock.patch('pyconfigify.configify._apply_config_file')
    @mock.patch('pyconfigify.configify._apply_cli_args')
    @mock.patch('pyconfigify.configify._apply_settings')
    def test_configify_with_cli_args_config_file_defined(self,
                       apply_settings_mock,
                       apply_cli_args_mock,
                       apply_config_file_mock,
                       load_config_file_mock,
                       get_config_file_path_mock,
                       parse_cli_args_mock,
                       add_implicit_settings_mock,
                       get_settings_mock,
                       ):
        """When _get_config_file_path returns path to config file then config file is loaded."""


        get_settings_mock.return_value = None
        add_implicit_settings_mock.return_value = None
        parse_cli_args_mock.return_value = None
        get_config_file_path_mock.return_value = "/some_path"
        load_config_file_mock.return_value = None
        apply_config_file_mock.return_value = None
        apply_cli_args_mock.return_value = None
        apply_settings_mock.return_value = None


        configs = Expando()
        configs.APP_NAME = Setting("MyApp", "Dummy App Name.")

        configify(configs)

        self.assertTrue(get_settings_mock.called)
        self.assertTrue(add_implicit_settings_mock.called)
        self.assertTrue(parse_cli_args_mock.called)
        self.assertTrue(get_config_file_path_mock.called)
        args, kwargs = get_config_file_path_mock.call_args
        self.assertEqual('config_file', args[1])
        self.assertTrue(load_config_file_mock.called)
        self.assertTrue(apply_config_file_mock.called)
        self.assertTrue(apply_cli_args_mock.called)
        self.assertTrue(apply_settings_mock.called)

    @mock.patch('pyconfigify.configify._get_settings')
    @mock.patch('pyconfigify.configify._add_implicit_settings')
    @mock.patch('pyconfigify.configify._parse_cli_args')
    @mock.patch('pyconfigify.configify._get_config_file_path')
    @mock.patch('pyconfigify.configify._load_config_file')
    @mock.patch('pyconfigify.configify._apply_config_file')
    @mock.patch('pyconfigify.configify._apply_cli_args')
    @mock.patch('pyconfigify.configify._apply_settings')
    def test_configify_without_cli_args_config_file_defined(self,
                                                         apply_settings_mock,
                                                         apply_cli_args_mock,
                                                         apply_config_file_mock,
                                                         load_config_file_mock,
                                                         get_config_file_path_mock,
                                                         parse_cli_args_mock,
                                                         add_implicit_settings_mock,
                                                         get_settings_mock,
                                                         ):
        """When _get_config_file_path does not return any path to config file then config file is not loaded."""

        get_settings_mock.return_value = None
        add_implicit_settings_mock.return_value = None
        parse_cli_args_mock.return_value = None
        get_config_file_path_mock.return_value = None
        load_config_file_mock.return_value = None
        apply_config_file_mock.return_value = None
        apply_cli_args_mock.return_value = None
        apply_settings_mock.return_value = None


        configs = Expando()
        configs.APP_NAME = Setting("MyApp", "Dummy App Name.")

        configify(configs)

        self.assertTrue(get_settings_mock.called)
        self.assertTrue(add_implicit_settings_mock.called)
        self.assertTrue(parse_cli_args_mock.called)
        self.assertTrue(get_config_file_path_mock.called)
        args, kwargs = get_config_file_path_mock.call_args
        self.assertEqual('config_file', args[1])
        self.assertFalse(load_config_file_mock.called)
        self.assertTrue(apply_config_file_mock.called)
        self.assertTrue(apply_cli_args_mock.called)
        self.assertTrue(apply_settings_mock.called)

    @mock.patch('pyconfigify.configify._get_settings')
    @mock.patch('pyconfigify.configify._add_implicit_settings')
    @mock.patch('pyconfigify.configify._parse_cli_args')
    @mock.patch('pyconfigify.configify._get_config_file_path')
    @mock.patch('pyconfigify.configify._load_config_file')
    @mock.patch('pyconfigify.configify._apply_config_file')
    @mock.patch('pyconfigify.configify._apply_cli_args')
    @mock.patch('pyconfigify.configify._apply_settings')
    def test_configify_with_cli_config_file(self,
                                                         apply_settings_mock,
                                                         apply_cli_args_mock,
                                                         apply_config_file_mock,
                                                         load_config_file_mock,
                                                         get_config_file_path_mock,
                                                         parse_cli_args_mock,
                                                         add_implicit_settings_mock,
                                                         get_settings_mock,
                                                         ):
        """When cli_config_file argument is specified, then it is a cli config file arg name."""


        get_settings_mock.return_value = None
        add_implicit_settings_mock.return_value = None
        parse_cli_args_mock.return_value = None
        get_config_file_path_mock.return_value = "/some_path"
        load_config_file_mock.return_value = None
        apply_config_file_mock.return_value = None
        apply_cli_args_mock.return_value = None
        apply_settings_mock.return_value = None


        configs = Expando()
        configs.APP_NAME = Setting("MyApp", "Dummy App Name.")

        configify(configs, 'cfg_file')

        self.assertTrue(get_settings_mock.called)
        self.assertTrue(add_implicit_settings_mock.called)
        self.assertTrue(parse_cli_args_mock.called)
        self.assertTrue(get_config_file_path_mock.called)
        args, kwargs = get_config_file_path_mock.call_args
        self.assertEqual('cfg_file', args[1])
        self.assertTrue(load_config_file_mock.called)
        self.assertTrue(apply_config_file_mock.called)
        self.assertTrue(apply_cli_args_mock.called)
        self.assertTrue(apply_settings_mock.called)


if __name__ == '__main__':
    unittest.main()












