funcsigs==1.0.2
linecache2==1.0.0
mock==2.0.0
pbr==1.10.0
pkg-resources==0.0.0
PyYAML==3.11
six==1.10.0
traceback2==1.4.0
unittest2==1.1.0
