import sys
from pyconfigify.configify import configify, Setting
this = sys.modules[__name__]

this.APP_NAME = Setting("TestScript", "Application name.")
this.APP_HOME = Setting("/opt/testscript", "Application base directory.")
this.POOL_SIZE = Setting(7, "Dummy pool size parameter of int type")


def main():

    configify(this, 'custom_config_file')
    print_script_vars()

def print_script_vars():
    print "APP_NAME: {} APP_HOME: {} POOL_SIZE: {}".format(this.APP_NAME, this.APP_HOME, this.POOL_SIZE)

if __name__ == "__main__":
    main()

