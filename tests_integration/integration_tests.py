import unittest2 as unittest
from os import path, environ
from subprocess import Popen, PIPE

class TestsIntegration(unittest.TestCase):
    """configify integration tests.

    Executes system under test as standalone process and validates configifying results.
    """

    def setUp(self):
        self.tests_file_dir = path.abspath(path.dirname(__file__))
        self.venw_path = path.join(path.dirname(self.tests_file_dir), 'venv', 'bin', 'python')

    def test_simple_configify_client_script_without_cli_args_and_config_file(self):
        """Configifyies python script without specifying any cli args or config file."""

        output, err, rc = self._act('client_cli_opt_args.py')

        self.assertEqual("APP_NAME: TestScript APP_HOME: /opt/testscript POOL_SIZE: 7 APP_SWITCH: None", output.rstrip())
        self.assertEqual(0, rc)

    def test_configify_client_script_with_cli_args_with_switch_off(self):
        """Configifyies python script with specifying cli args.

        Cli args are optional and defined in both ways - by full name and short name."""

        output, err, rc = self._act('client_cli_opt_args.py', '--app_name', 'NewTestScript', '-ah', '/opt/new_testscript')

        self.assertEqual("APP_NAME: NewTestScript APP_HOME: /opt/new_testscript POOL_SIZE: 7 APP_SWITCH: None", output.rstrip())
        self.assertEqual(0, rc)

    def test_configify_client_script_with_cli_args_with_switch_on(self):
        """Configifyies python script with specifying cli args.

        Cli args are optional and defined in both ways - by full name and short name."""

        output, err, rc = self._act('client_cli_opt_args.py', '--app_name', 'NewTestScript', '-ah', '/opt/new_testscript', '--app_switch')

        self.assertEqual("APP_NAME: NewTestScript APP_HOME: /opt/new_testscript POOL_SIZE: 7 APP_SWITCH: --debug", output.rstrip())
        self.assertEqual(0, rc)

    def test_configify_client_script_with_config_file_by_full_path(self):
        """Configifyies python script with config file.

        Cli arg specifies config file full path."""

        config_file = path.join(path.dirname(path.realpath(__file__)), 'resources', 'config_simple.yaml')
        output, err, rc = self._act('client_cli_opt_args.py', '--config_file', config_file)

        self.assertEqual("APP_NAME: app_name_from_file APP_HOME: /app_home/from_file POOL_SIZE: 7 APP_SWITCH: None", output.rstrip())
        self.assertEqual(0, rc)

    def test_configify_client_script_with_config_file_by_relative_path(self):
        """Configifyies python script with config file.

        Cli arg specifies config file relative path."""

        output, err, rc = self._act('client_cli_opt_args.py', '--config_file', 'config_simple.yaml')

        self.assertEqual("APP_NAME: app_name_from_file APP_HOME: /app_home/from_file POOL_SIZE: 7 APP_SWITCH: None", output.rstrip())
        self.assertEqual(0, rc)

    def test_configify_client_script_with_custom_config_file_by_full_path_and_cli_args(self):
        """Configifyies script with config file and cli args. Config file itself is defined by custom cli arg.

        One setting is set by config file is overwritten by cli,
        one default setting is overwritten by cli,
        one setting is set by config file (not overwritten by cli)."""

        config_file = path.join(path.dirname(path.realpath(__file__)), 'resources', 'config_simple.yaml')
        output, err, rc = self._act('client_cli_opt_args_n_custom_config_file_cli_arg.py',
                                    '--custom_config_file', config_file,
                                    '-ps', '77',
                                    '-an', 'app_name_from_cli')

        self.assertEqual("APP_NAME: app_name_from_cli APP_HOME: /app_home/from_file POOL_SIZE: 77", output.rstrip())
        self.assertEqual(0, rc)

    def test_configify_client_script_with_custom_config_file_by_full_path_and_cli_positional_args(self):
        """Configifyies script with config file and positional cli args.

        There are 2 positional arguments and one optional.
        """

        config_file = path.join(path.dirname(path.realpath(__file__)), 'resources', 'config_simple.yaml')
        output, err, rc = self._act('client_cli_positional_args.py',
                                    'restart', '3', '-cf', config_file)

        self.assertEqual("COMMAND: restart LOG_LEVEL: 3 MESSAGE: message_from_config_file", output.rstrip())
        self.assertEqual(0, rc)

    def _act(self, script, *args):
        """Act method of AAA."""

        sut = [self.venw_path, self._get_script_path(script)] + (list(args or []))

        p = Popen(sut, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        output, err = p.communicate(b"input data that is passed to subprocess' stdin")
        rc = p.returncode

        return output, err, rc

    def _get_script_path(self, script_name):

        sut = path.join(self.tests_file_dir, 'resources', script_name).strip()
        return sut

